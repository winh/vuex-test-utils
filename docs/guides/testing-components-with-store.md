## Testing components with store

In the following example we assume you have a component method which dispatches an action:

```javascript
export default {
  // ...

  methods: {
    buttonClickHandler() {
      this.$store.dispatch('doSomething', 42)
    }
  }
}
```

The following shows how a test for this method looks like.
For mounting the component we are relying on [`mount() from Vue Test Utils](https://vue-test-utils.vuejs.org/api/#mount).

```javascript
import { mount } from '@vue/test-utils'

import { createStoreWrapper } from '@winh/vuex-test-utils'

import store from './store'
import Component from './component'

describe('Component', () => {
  test('clicking button triggers an action', () => {
    const storeWrapper = createStoreWrapper(store, { shallowActions: true });
    const wrapper = mount(Component, { store });
    const action = storeWrapper.waitForAction('doSomething');

    const buttonWrapper = wrapper.find('.some-btn')
    buttonWrapper.trigger('click')

    return action.then(({ payload }) => {
      expect(storeWrapper.actionHistory.length).toBe(1)
      expect(payload).toBe(42)      
    });
  })
})
```

The `shallowActions` parameter causes the actions of the store to be replaced with mocks which do nothing.
This ensures we are testing the component in isolation from the store.
