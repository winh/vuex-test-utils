## Testing mutations

In the following example we assume you have a store with a mutation called `SERVE_PIZZA`.

```javascript
export default {
  state: {
    pizzaCount: 0
  },

  mutations: {
    SERVE_PIZZA(state, amountOfPizza) {
      state.pizzaCount += amountOfPizza
    }
  }
}
```

The test for this mutation looks like this:

```javascript
import { createStoreWrapper } from '@winh/vuex-test-utils'

import store from './store'

describe('Store mutations', () => {
  describe('SERVE_PIZZA', () => {
    test('adds new pizza', () => {
      const numberOfPizzas = 9000
      const storeWrapper = createStoreWrapper(store)
      storeWrapper.state.pizzaCount = 1

      storeWrapper.commit('SERVE_PIZZA', numberOfPizzas)

      expect(storeWrapper.state.pizzaCount).toBe(numberOfPizzas + 1)
    })
  })
})
```
