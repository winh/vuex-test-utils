## Testing actions

In the following example we assume you have a store with an action called `makeRain`.

```javascript
export default {
  // ...

  actions: {
    makeRain({ commit }, numberOfDrops) {
      commit('SET_WATER_LEVEL', numberOfDrops)
    }
  }
}
```

The test for this action looks like this:

```javascript
import { createStoreWrapper } from '@winh/vuex-test-utils'

import store from './store'

describe('Store actions', () => {
  describe('makeRain', () => {
    test('sets the water level', () => {
      const someRain = 23
      const storeWrapper = createStoreWrapper(store, { shallowMutations: true })

      storeWrapper.dispatch('makeRain', someRain)

      expect(storeWrapper.mutationHistory.length).toBe(1)
      const { type, payload } = storeWrapper.mutationHistory[0]
      expect(type).toBe('SET_WATER_LEVEL')
      expect(payload).toBe(someRain)
    })
  })
})
```

The `shallowMutations` parameter causes the mutations of the store to be replaced with mocks which do nothing.
This ensures we are testing the actions in isolation from the mutations.

### Asynchronous actions

For asynchronous actions you can use the `Promise` returned by `storeWrapper.dispatch()` to wait for its completion:

```javascript
export default {
  // ...

  actions: {
    makeHeavyRain({ commit }, numberOfDrops) {
      return Promise.resolve(2 * numberOfDrops)
        .then(waterLevel => commit('SET_WATER_LEVEL', waterLevel))
    }
  }
}
```

```javascript
import { createStoreWrapper } from '@winh/vuex-test-utils'

import store from './store'

describe('Store actions', () => {
  describe('makeHeavyRain', () => {
    test('sets the water level', done => {
      const someRain = 23
      const storeWrapper = createStoreWrapper(store, { shallowMutations: true })

      storeWrapper.dispatch('makeHeavyRain', someRain)
        .then(() => {
          expect(storeWrapper.mutationHistory.length).toBe(1)
          const { type, payload } = storeWrapper.mutationHistory[0]
          expect(type).toBe('SET_WATER_LEVEL')
          expect(payload).toBe(2 * someRain)          
        })
        .then(done)
        .catch(done.fail)
    })
  })
})
```