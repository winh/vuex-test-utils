# Introduction

Vuex Test Utils is the inofficial unit testing utility library for Vuex.
It is heavily inspired by [Vue Test Utils].

**Note: The implementation is still in a very early stage and may change any time.**


* [Guides](guides/)
  * [Testing components with store](guides/testing-components-with-store.md)
  * [Testing actions](guides/testing-actions.md)
  * [Testing mutations](guides/testing-mutations.md)
* [API](api/)
  * [createStoreWrapper](api/create-store-wrapper.md)
  * [StoreWrapper](api/store-wrapper/)
    * [commit](api/store-wrapper/commit.md)
    * [dispatch](api/store-wrapper/dispatch.md)
    * [options](api/store-wrapper/options.md)
    * [properties](api/store-wrapper/properties.md)
    * [waitForAction](api/store-wrapper/wait-for-action.md)
    * [waitForMutation](api/store-wrapper/wait-for-mutation.md)
* [Repository](https://gitlab.com/winh/vuex-test-utils)
* [Upstream Issue](https://github.com/vuejs/vue-test-utils/issues/1060)

[Vue Test Utils]: https://vue-test-utils.vuejs.org/
