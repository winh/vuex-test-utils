## createWrapper(store [, options])

- **Arguments:**

  - `{store|Vuex.Store} store`
  - `{Object} options`

- **Returns:**
  - `{StoreWrapper}`

`store` can be a [`Vuex.Store` instance] or a factory function returning a store instance.
The supported `options` are the same as [for `StoreWrapper`](store-wrapper/#options).

[`Vuex.Store` instance]: https://vuex.vuejs.org/api/#vuex-store
