### dispatch()

This is the same as [`store.dispatch()`](https://vuex.vuejs.org/api/#dispatch) and provided for convenience.
