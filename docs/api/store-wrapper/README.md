# StoreWrapper

Vuex Test Utils has a wrapper based API similar to [Vue Test Utils].

A `StoreWrapper` is an object that contains a [Vuex store] instance and methods to help testing the store or a component using it.

[Vue Test Utils]: https://vue-test-utils.vuejs.org/
[Vuex store]: https://vuex.vuejs.org/

!!!include(docs/api/store-wrapper/options.md)!!!
!!!include(docs/api/store-wrapper/properties.md)!!!

## Methods

!!!include(docs/api/store-wrapper/commit.md)!!!
!!!include(docs/api/store-wrapper/dispatch.md)!!!
!!!include(docs/api/store-wrapper/wait-for-action.md)!!!
!!!include(docs/api/store-wrapper/wait-for-mutation.md)!!!
