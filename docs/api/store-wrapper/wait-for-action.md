### waitForAction(type [, options])

- **Arguments:**

  - `{String} type` name of the action to wait for
  - `{Object} options`
    - `{Number} timeout` maximum number of milliseconds to wait

- **Returns:**
  - `{Promise}`

Returns a `Promise` that resolves after the given action type was dispatched or rejects after the timeout has exceeded.
