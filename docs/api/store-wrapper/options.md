## Options

Options for `createStoreWrapper` and `StoreWrapper`.

- [`shallowActions`](#shallowactions)
- [`shallowMutations`](#shallowmutations)

### `shallowActions`

- type: `Boolean` (default: `false`)

If set to `true`, all store actions are replaced with mocks that do nothing.
This is helpful for unit testing Vue components.

### `shallowMutations`

- type: `Boolean` (default: `false`)

If set to `true`, all store mutations are replaced with mocks that do nothing.
This is helpful for unit testing Vuex store actions.
