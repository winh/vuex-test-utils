### commit()

This is the same as [`store.commit()`](https://vuex.vuejs.org/api/#commit) and provided for convenience.
