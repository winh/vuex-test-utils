## Properties

### `state`

- type: `Object` (read-only)

This is the same as `store.state` and provided for convenience.

### `store`

- type: `Vuex.Store` (read-only)

This is the wrapped store instance passed to the contructor.
You can access all the [instance methods](https://vuex.vuejs.org/api/#vuex-store-instance-methods) and [properties of a store](https://vuex.vuejs.org/api/#vuex-store-instance-properties).


### `actionHistory`

- type: `Array<Object>`

Stores all actions which are dispatched after wrapping the store instance.

### `mutationHistory`

- type: `Array<Object>`

Stores all mutations which are committed after wrapping the store instance.
