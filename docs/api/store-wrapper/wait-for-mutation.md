### waitForMutation(type [, options])

- **Arguments:**

  - `{String} type` name of the mutation to wait for
  - `{Object} options`
    - `{Number} timeout` maximum number of milliseconds to wait

- **Returns:**
  - `{Promise}`

Returns a `Promise` that resolves as soon as the given mutation was committed and rejects after the timeout has exceeded.
