module.exports = {
  themeConfig: {
    algolia: {
      apiKey: 'aeb7ad1eab28d97f7e8186056e0742f6',
      indexName: 'vuex_test_utils',
    },
    locales: {
      '/': {
        label: 'English',
        selectText: 'Languages',
        sidebar: ['/', '/guides/', '/api/', '/api/store-wrapper/'],
      },
    },
  },
  base: '/vuex-test-utils/',
  dest: 'public',
  markdown: {
    extendMarkdown: md => {
      md.use(require('markdown-it-include'));
    },
  },
};
