/* eslint-disable filenames/match-regex */

module.exports = {
  presets: [['@babel/preset-env', { modules: false }]],

  env: {
    test: {
      plugins: ['transform-es2015-modules-commonjs'],
    },
  },
};
