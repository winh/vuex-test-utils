import Vuex from 'vuex'; // eslint-disable-line import/no-unresolved

import { StoreWrapper } from './index';

const createStoreWrapper = (store, options = {}) => {
  let storeInstance = store;
  if (store instanceof Function) {
    storeInstance = store();
  }

  if (!(storeInstance instanceof Vuex.Store)) {
    throw new Error('store needs to be an instance of Vuex.Store or return one!');
  }

  const storeWrapper = new StoreWrapper(storeInstance, options);
  return storeWrapper;
};

export default createStoreWrapper;
