const fromEntries = require('object.fromentries');

const addListener = (listenersMap, type, options) => {
  const timeoutInMilliseconds = (options && options.timeout) || 1000;
  let timeoutHandle;

  const promise = new Promise((resolve, reject) => {
    const errorCallback = () => {
      const error = new Error(`Waiting for ${type} exceeded ${timeoutInMilliseconds}ms!`);
      reject(error);
    };

    timeoutHandle = setTimeout(errorCallback, timeoutInMilliseconds);
    listenersMap[type].push(resolve);
  }).then((...args) => {
    clearTimeout(timeoutHandle);
    return Promise.resolve(...args);
  });

  return promise;
};

const callListeners = (listenersMap, receivedObject) => {
  const listeners = listenersMap[receivedObject.type];
  if (listeners) {
    listeners.forEach(listener => listener(receivedObject));
  }
};

const createMocksObject = originalObject => {
  const keys = Object.keys(originalObject);
  const mockEntries = keys.map(type => [type, () => {}]);
  return fromEntries(mockEntries);
};

const throwError = errorMessage => {
  throw new Error(errorMessage);
};

export default class StoreWrapper {
  constructor(storeInstance, options = {}) {
    this.actionHistory = [];
    this.actionListeners = {};
    this.mutationHistory = [];
    this.mutationListeners = {};

    Object.defineProperty(this, 'store', {
      get: () => storeInstance,
      set: () => throwError('storeWrapper.store is read-only'),
    });
    Object.defineProperty(this, 'state', {
      get: () => this.store.state,
      set: () => throwError('storeWrapper.state is read-only'),
    });

    storeInstance.subscribe(mutation => this.addMutationToHistory(mutation));
    storeInstance.subscribeAction({ after: action => this.addActionToHistory(action) });

    if (options && options.shallowActions) {
      this.mockActions();
    }

    if (options && options.shallowMutations) {
      this.mockMutations();
    }
  }

  addActionToHistory(action) {
    this.actionHistory.push(action);
    callListeners(this.actionListeners, action);
    delete this.actionListeners[action.type];
  }

  addMutationToHistory(mutation) {
    this.mutationHistory.push(mutation);
    callListeners(this.mutationListeners, mutation);
    delete this.mutationListeners[mutation.type];
  }

  commit(...args) {
    return this.store.commit(...args);
  }

  dispatch(...args) {
    return this.store.dispatch(...args);
  }

  mockActions() {
    const { _actions: originalActions } = this.store;
    const mockedActions = createMocksObject(originalActions);
    return this.store.hotUpdate({ actions: mockedActions });
  }

  mockMutations() {
    const { _mutations: originalMutations } = this.store;
    const mockedMutations = createMocksObject(originalMutations);
    return this.store.hotUpdate({ mutations: mockedMutations });
  }

  waitForAction(type, options = {}) {
    if (!this.actionListeners[type]) {
      this.actionListeners[type] = [];
    }

    return addListener(this.actionListeners, type, options);
  }

  waitForMutation(type, options = {}) {
    if (!this.mutationListeners[type]) {
      this.mutationListeners[type] = [];
    }

    return addListener(this.mutationListeners, type, options);
  }
}
