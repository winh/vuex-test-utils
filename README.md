# vuex-test-utils

Vuex Test Utils is the inofficial unit testing utility library for Vuex.
It is heavily inspired by [Vue Test Utils].

**Note: The implementation is still in a very early stage and may change any time.**

You can find the corresponding upstream issue at https://github.com/vuejs/vue-test-utils/issues/1060.
