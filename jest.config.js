/* eslint-disable filenames/match-regex */

module.exports = {
  moduleFileExtensions: ['js', 'json', 'vue'],
  transform: {
    '^.+\\.js$': 'babel-jest',
    '^.+\\.vue$': 'vue-jest',
  },
  collectCoverage: true,
  collectCoverageFrom: ['src/*.js'],
  coverageReporters: ['text-summary', 'lcov'],
};
