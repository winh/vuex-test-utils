/* eslint-disable filenames/match-regex */

import Vue from 'vue';
import Vuex from 'vuex';
import { StoreWrapper } from '..';

describe('StoreWrapper', () => {
  let someAction;
  let someMutation;
  let store;

  beforeEach(() => {
    someAction = jest.fn();
    someMutation = jest.fn();

    Vue.use(Vuex);
    store = new Vuex.Store({
      actions: {
        someAction,
      },
      mutations: {
        someMutation,
      },
    });
  });

  describe('constructor', () => {
    describe('without storeInstance', () => {
      it('throws an error', () => {
        expect(() => new StoreWrapper()).toThrow();
      });
    });

    describe('without options', () => {
      let storeWrapper;

      beforeAll(() => {
        storeWrapper = new StoreWrapper(store);
      });

      it('does not mock actions', () => {
        const action = store.dispatch('someAction');

        return action.then(() => {
          expect(someAction).toHaveBeenCalled();
        });
      });

      it('does not mock mutations', () => {
        store.commit('someMutation');
        expect(someMutation).toHaveBeenCalled();
      });

      it('creates an empty actionHistory', () => {
        expect(storeWrapper.actionHistory).toEqual([]);
      });

      it('creates an empty mutationHistory', () => {
        expect(storeWrapper.mutationHistory).toEqual([]);
      });
    });
  });
});
