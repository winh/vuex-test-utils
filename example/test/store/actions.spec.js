/* eslint-disable filenames/match-regex */
import { createStoreWrapper } from '../../..';

import storeFactory from '../../todomvc/store';

describe('Store actions', () => {
  let storeWrapper;

  beforeEach(() => {
    storeWrapper = createStoreWrapper(storeFactory, { shallowMutations: true });
  });

  describe('addTodo', () => {
    it('commits ADD_TODO', () => {
      const dummyTodo = 'buy a pony';

      storeWrapper.dispatch('addTodo', dummyTodo);

      expect(storeWrapper.mutationHistory.length).toBe(1);
      const [mutation] = storeWrapper.mutationHistory;
      expect(mutation.type).toBe('ADD_TODO');
      expect(mutation.payload).toBe(dummyTodo);
    });
  });

  describe('allDone', () => {
    it.each`
      todos                     | expectedValue
      ${[]}                     | ${true}
      ${[{ completed: false }]} | ${false}
      ${[{ completed: true }]}  | ${true}
    `('commits FILTER_TODOS with $expectedValue for $todos', ({ todos, expectedValue }) => {
      Object.assign(storeWrapper.state, { todos });

      storeWrapper.dispatch('allDone');

      expect(storeWrapper.mutationHistory.length).toBe(1);
      const [mutation] = storeWrapper.mutationHistory;
      expect(mutation.type).toBe('FILTER_TODOS');
      expect(mutation.payload).toBe(expectedValue);
    });
  });
});
