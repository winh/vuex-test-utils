/* eslint-disable filenames/match-regex */

import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueRouter from 'vue-router'; // eslint-disable-line import/no-extraneous-dependencies

import { createStoreWrapper } from '../../..';

import storeFactory from '../../todomvc/store';
import Header from '../../todomvc/components/header.vue';

const localVue = createLocalVue();
localVue.use(VueRouter);
const router = new VueRouter();

describe('Header', () => {
  const dummyTodo = 'some important task';

  const wrap = () => {
    const storeWrapper = createStoreWrapper(storeFactory, { shallowActions: true });

    const { store } = storeWrapper;
    const wrapper = shallowMount(Header, {
      localVue,
      router,
      store,
    });

    return { wrapper, storeWrapper };
  };

  it('adds a new todo', () => {
    const { wrapper, storeWrapper } = wrap();
    const action = storeWrapper.waitForAction('addTodo');

    const inputElement = wrapper.find('.new-todo');
    inputElement.setValue(dummyTodo);
    inputElement.trigger('keyup.enter');

    return action.then(({ payload }) => {
      expect(storeWrapper.actionHistory.length).toBe(1);
      expect(payload.title).toBe(dummyTodo);
    });
  });
});
